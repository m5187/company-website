var $root = $('html, body');

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

$(document).ready(() => {

  $(document).on('click', '#menu_checkbox_label', function(){
    var open = Boolean($('#menu_checkbox').prop('checked'));

    if (open){

      $('.sidemenu').animate({
        width: '-=100'
      }, 100);
      $( ".menu-links" ).hide(100);

    } else {

      $('.sidemenu').animate({
        width: '+=100'
      }, 100);
      $( ".menu-links" ).show(100);

    }
    $('#menu_checkbox').prop('checked', true ? open : !open)
  });

  $(document).on('click', '.sidemenu-mobile .menu-wrapper', function(){

    var open = Boolean($(this).prop('data-open'));

    if (open){
      $( ".menu-links" ).hide(100);
    } else {
      $( ".menu-links" ).show(100);
    }

    $(this).prop('data-open', !open)
  });

  $('.contact-button').click(() => {

    $('.contact-form-holder').css("display", "block");

  });

  $('.close-form').click(() => {

    $('.contact-form-holder').css("display", "none");

  });

  $('.contact-form button').click(() => {
    let mail = $('.contact-form input').prop('value');
    let message = $('.contact-form textarea').prop('value');

    if (!mail || !message){
      siiimpleToast.message('Fields missing');
      return
    }

    if(!validateEmail(mail)){
      siiimpleToast.message('Email format is wrong');
      return
    }

    var emailEncrypted = 'dxxavexx@meanxxxunixxcorxxnsxx.cxxom'

    Email.send({
        SecureToken : "dffa807c-02c2-4465-8e25-8f3274700b65",
        To : emailEncrypted.replace(/x/g, ''),
        From : "meanunicorns@meanunicorns.com",
        Subject : "New Contact Attempt",
        Body : `From "${mail}". \n Request content: \n ${message}`
    }).then(
      message => console.log(message)
    );

    siiimpleToast.message('Message sent. We will get back to you soon!');

    $('.contact-form-holder').css("display", "none");

    mail.prop('value', '');
    message.prop('value', '');


  });

  $(document).on('click', '.menu-links p', function (event) {
    var href = $(this).attr('data-pointer');

    $root.animate({
        scrollTop: $(href).offset().top
    }, 500);

    $('.sidemenu').animate({
      width: '-=100'
    }, 100);
    $( ".menu-links" ).hide(100);

    $('#menu_checkbox').prop('checked', false);
    $('.sidemenu-mobile .menu-wrapper').prop('data-open', false);

});

});
